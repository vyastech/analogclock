package com.example.vyashole.analogclock.model;

import java.util.Calendar;

/**
 * Provides methods for analog clock
 */

public class AnalogClock {

    public static final float HOURS_ON_CLOCK = 12,
                            MINUTES_ON_CLOCK = 60,
                            SECONDS_ON_CLOCK = 60,
                            DEGREES_IN_ROTATION = 360;

    private float hourOffsetDeg, minuteOffsetDeg, secondOffsetDeg;

    public AnalogClock(float hourOffsetDeg, float minuteOffsetDeg, float secondOffsetDeg) {
        this.hourOffsetDeg = hourOffsetDeg;
        this.minuteOffsetDeg = minuteOffsetDeg;
        this.secondOffsetDeg = secondOffsetDeg;
    }

    public float getHours() {
        return Calendar.getInstance().get(Calendar.HOUR_OF_DAY) % HOURS_ON_CLOCK;
    }

    public float getMinutes() {
        return Calendar.getInstance().get(Calendar.MINUTE);
    }

    public float getSeconds() {
        return Calendar.getInstance().get(Calendar.SECOND);
    }

    public float getHourHandRoataion() {
        return (getHours() * DEGREES_IN_ROTATION / HOURS_ON_CLOCK) + hourOffsetDeg;
    }

    public float getMinuteHandRotation() {
        return (getMinutes() * DEGREES_IN_ROTATION / MINUTES_ON_CLOCK) + minuteOffsetDeg;
    }

    public float getSecondHandRotation() {
        return (getSeconds() * DEGREES_IN_ROTATION / SECONDS_ON_CLOCK) + secondOffsetDeg;
    }
}
