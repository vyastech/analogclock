package com.example.vyashole.analogclock;

import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.example.vyashole.analogclock.model.AnalogClock;

public class MainActivity extends AppCompatActivity {

    private ImageView hourHand, minuteHand, secondHand;
    private AnalogClock analogClock;
    private Handler clockHandler;

    private static int FIRST_TICK = 0,
                        SUBSEQUENT_TICKS = 1,
                        UPDATE_INTERVAL = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        hourHand = (ImageView) findViewById(R.id.hourHand);
        minuteHand = (ImageView) findViewById(R.id.minuteHand);
        secondHand = (ImageView) findViewById(R.id.secondHand);

        // Initialize the clock with the initial rotation values of each hand
        analogClock = new AnalogClock( hourHand.getRotation(),
                                    minuteHand.getRotation(),
                                     secondHand.getRotation());


        clockHandler  = new Handler(new Handler.Callback() {
            @Override
            public boolean handleMessage(Message msg) {

                if(msg.what == FIRST_TICK || analogClock.getSeconds() == 0 && analogClock.getMinutes() == 0)
                    hourHand.setRotation(analogClock.getHourHandRoataion());

                if(msg.what== FIRST_TICK || analogClock.getSeconds()== 0)
                    minuteHand.setRotation(analogClock.getMinuteHandRotation());

                secondHand.setRotation(analogClock.getSecondHandRotation());
                clockHandler.sendEmptyMessageDelayed(SUBSEQUENT_TICKS, UPDATE_INTERVAL);
                return true;
            }
        });

        clockHandler.sendEmptyMessage(FIRST_TICK);

    }
}
